---
author: Benoît Launay (Sui Generis)
title: Les Angles
collection: pagedjs × editoria 2022
app: editoria
cover: cover.png
pdfpreview: preview.pdf
sources: "https://gitlab.coko.foundation/pagedjs/pagedjs-templates/les-angles"

---

# Theme name: Les angles

## Typefaces

Both typefaces used come from the *Velvetyne*[^velvetyne] foundry.

### Karrik

- titles

### Happy Times New Game

- main content

## References

*Les Angles* is a french Pyrenean winter and summer resort station [its official]

[^velvetyne]: *Velvetyne* is a French type foundry which designs and distributes free and open-source typefaces. [more information on their *about* page](https://velvetyne.fr/about/)