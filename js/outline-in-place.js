class outlines extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    content.querySelectorAll("section.level-two").forEach((section) => {
      if (!section.querySelector(".outline")) {
        return
      }
      let outlineOutput = document.createElement("ol");
      outlineOutput.classList.add("outline-remake");
      section.querySelectorAll("section > h2").forEach((h2) => {
        console.log(h2)
        outlineOutput.insertAdjacentHTML(
          `beforeend`,
          `<li>${h2.innerHTML}</li>`
        );
      });
    
        section.querySelector(".outline").innerHTML = outlineOutput.outerHTML;
    });
  }
}
Paged.registerHandlers(outlines);
